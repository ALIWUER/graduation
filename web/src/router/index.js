import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '../views/LoginView.vue'
import RegisterView from '../views/RegisterView.vue'
import HomeView from '../views/Manager.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: LoginView
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView
    },
    {
      path: '/home',
      name: 'home',
      component: HomeView,
      children: [
        { path: 'subject', name: 'subject', meta: { name: '已选题目' }, component: () => import('../views/manager/Subject.vue') },
        { path: 'addTopic', name: 'addTopic', meta: { name: '教师出题' }, component: () => import('../views/manager/Add.vue') },
        { path: 'select', name: 'select', meta: { name: '学生选题' }, component: () => import('../views/manager/Select.vue') },
        { path: 'topicInfo', name: 'topicInfo', meta: { name: '学生已选题' }, component: () => import('../views/manager/TopicInfo.vue') },
      ]
    },

  ]
})

export default router
