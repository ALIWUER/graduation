import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import VueDevTools from 'vite-plugin-vue-devtools'

// https://vitejs.dev/config/
export default defineConfig({
  devServer:{
    proxy: {
      '/api': {
        traget: 'http://localhost:8081',
        changeOrigin: true,
        ws: true,
        pathRewrite: { '^/api': '' }
      }
    },
    port: 8082
  },
  plugins: [
    vue(),
    VueDevTools(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})

