package seig.wjy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import seig.wjy.domain.pojo.GraduationTopic;
import seig.wjy.domain.pojo.User;
import seig.wjy.domain.qo.PageQO;
import seig.wjy.tools.ResponseResult;

import java.util.List;

/**
* @author wu
* @description 针对表【 graduation_topic】的数据库操作Service
* @createDate 2024-05-26 23:23:04
*/
public interface GraduationTopicService extends IService<GraduationTopic> {
    ResponseResult addTopic(GraduationTopic topic);
    IPage<GraduationTopic> list(PageQO qo);
    IPage<GraduationTopic> getList(PageQO qo);

}
