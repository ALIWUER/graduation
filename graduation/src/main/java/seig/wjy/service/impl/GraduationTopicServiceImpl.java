package seig.wjy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import seig.wjy.domain.pojo.User;
import seig.wjy.mapper.GraduationTopicMapper;
import seig.wjy.domain.pojo.GraduationTopic;
import seig.wjy.domain.qo.PageQO;
import seig.wjy.mapper.UserMapper;
import seig.wjy.service.GraduationTopicService;
import seig.wjy.tools.AppHttpCodeEnum;
import seig.wjy.tools.ResponseResult;
import seig.wjy.tools.SimilarityUtils;

import java.util.List;

/**
 * @author wu
 * @description 针对表【 graduation_topic】的数据库操作Service实现
 * @createDate 2024-05-26 23:23:04
 */
@Service
@Transactional
public class GraduationTopicServiceImpl extends ServiceImpl<GraduationTopicMapper, GraduationTopic>
        implements GraduationTopicService {

    @Autowired
    private GraduationTopicMapper graduationTopicMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean save(GraduationTopic topic) {
//        topic.setId((int) (super.count()+1));
        return super.save(topic);
    }

    @Override
    @Transactional
    public ResponseResult addTopic(GraduationTopic topic) {
        //查重率
        boolean flag = getCosineSimilarity(topic);
        if (flag) {
            return ResponseResult.errorResult(AppHttpCodeEnum.CONTENT_REPEAT_HIGH);
        }
        //保存
        try {
            boolean res = save(topic);
            if (res) {
                return ResponseResult.okResult("出题成功");
            } else {
                return ResponseResult.errorResult(AppHttpCodeEnum.TOPIC_ERROR);
            }
        } catch (Exception e) {
            return ResponseResult.errorResult(AppHttpCodeEnum.SYSTEM_ERROR);
        }

    }

    /**
     * TODO: 2024/5/26 实现查重率
     */
    private boolean getCosineSimilarity(GraduationTopic topic) {
        // 获取在数据库中的所有题目
        List<GraduationTopic> topics = super.list();
        // 计算相似度
        for (GraduationTopic t : topics) {
            float ratio = SimilarityUtils.getSimilarityRatio(topic.getTitleName(), t.getTitleName());
            //大于80%为重复
            if (ratio > 0.8) {
                return true;
            }
        }
        return false;
    }

    //分页查询
    @Override
    public IPage<GraduationTopic> list(PageQO qo) {
        //设置分页信息
        IPage<GraduationTopic> page = new Page<>(qo.getPageNum(), qo.getPageSize());
        //设置查询条件
        LambdaQueryWrapper<GraduationTopic> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(GraduationTopic::getStatus, 1);
        //分页查询
        page = baseMapper.selectPage(page, wrapper);
        return page;
    }


    public IPage<GraduationTopic> getList(PageQO qo) {
        List<GraduationTopic> graduationTopics = graduationTopicMapper.getTopics(qo.getPageNum() * qo.getPageSize(), qo.getPageSize());
//        List<GraduationTopic> graduationTopics=graduationTopicMapper.selectAll();
        IPage<GraduationTopic> page = new Page<>(qo.getPageNum(), qo.getPageSize());
//        int i = 0;
//        for (GraduationTopic g : graduationTopics) {
//            String teacherName = userMapper.getTeacherName(g.getTeacherId());
//            graduationTopics.get(i).setTeacherName(teacherName);
//            i++;
//        }
        for (GraduationTopic g : graduationTopics) {
            String teacherName = userMapper.getTeacherName(g.getTeacherId());
            g.setTeacherName(teacherName);
        }
        page.setRecords(graduationTopics);
        //设置总条数
        Long count = graduationTopicMapper.selectCount(null);
        page.setTotal(count);
        return page;
    }

}




