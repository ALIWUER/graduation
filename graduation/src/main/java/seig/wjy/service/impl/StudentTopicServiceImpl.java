package seig.wjy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import seig.wjy.domain.pojo.GraduationTopic;
import seig.wjy.domain.pojo.Information;
import seig.wjy.mapper.GraduationTopicMapper;
import seig.wjy.mapper.StudentTopicMapper;
import seig.wjy.mapper.UserMapper;
import seig.wjy.service.StudentTopicService;
import seig.wjy.tools.AppHttpCodeEnum;
import seig.wjy.tools.ResponseResult;

@Service
public class StudentTopicServiceImpl extends ServiceImpl<StudentTopicMapper,Information> implements StudentTopicService {
    @Autowired
    private StudentTopicMapper studentTopicMapper;
    @Autowired
    private GraduationTopicMapper graduationTopicMapper;
    @Autowired
    private UserMapper userMapper;


    @Override
    public ResponseResult addStudentTopic(Information information) {
        Information i=studentTopicMapper.selectOne(information.getUserId());
        if (i!=null){
            return ResponseResult.errorResult(AppHttpCodeEnum.IS_SELECT);
        }
        information.setStudentId(information.getUserId());
        boolean res = save(information);
        if (res){
            graduationTopicMapper.updateStatus(information.getTitleId());
           return ResponseResult.okResult();
        }else {
            return ResponseResult.errorResult(AppHttpCodeEnum.TOPIC_ERROR);
        }

    }

    @Override
    public GraduationTopic getStudentTopic(Integer userId) {
        Information i=studentTopicMapper.selectOne(userId);
        if (i!=null){
            GraduationTopic graduationTopic=graduationTopicMapper.selectOne(i.getTitleId());
            String teacherName = userMapper.getTeacherName(graduationTopic.getTeacherId());
            graduationTopic.setTeacherName(teacherName);
            return graduationTopic;
        }
        return null;
    }
    @Override
    @Transactional
    public ResponseResult deleteTopic(Integer userId) {
        try {
            Information information = studentTopicMapper.selectOne(userId);
            int i = studentTopicMapper.deleteByUserId(userId);
            int titleId = information.getTitleId();
            graduationTopicMapper.updateStatus1(titleId);
            if (i>0){
                return ResponseResult.okResult();
            }else {
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_DELETE_ERROR);
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
            return ResponseResult.errorResult(AppHttpCodeEnum.ERROR);
        }
    }
}
