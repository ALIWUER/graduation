package seig.wjy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import seig.wjy.domain.pojo.User;
import seig.wjy.domain.vo.LoginUser;
import seig.wjy.mapper.UserMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author wu
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 根据用户名查询用户信息
        // 查询数据库
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        User user = userMapper.selectOne(queryWrapper);
        // 判断用户是否存在
        if(Objects.isNull(user)){
            System.out.println("UserDetailsServiceImpl:用户名不存在");
        }
        //权限认证
        List<String> list = new ArrayList<>();
        String auth = userMapper.getAuth(username);
        list.add(auth);
        // 返回UserDetails对象
        return new LoginUser(user,list);
    }
}
