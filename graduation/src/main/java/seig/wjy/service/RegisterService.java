package seig.wjy.service;


import com.baomidou.mybatisplus.extension.service.IService;
import seig.wjy.domain.pojo.User;
import seig.wjy.tools.ResponseResult;

/**
 * @author wu
 */
public interface RegisterService extends IService<User> {
    public ResponseResult register(User user);
}
