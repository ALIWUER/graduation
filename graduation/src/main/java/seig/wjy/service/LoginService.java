package seig.wjy.service;


import com.baomidou.mybatisplus.extension.service.IService;
import seig.wjy.domain.pojo.User;

public interface LoginService extends IService<User> {
    String login(User user);
    User selectUser(String username);
}
