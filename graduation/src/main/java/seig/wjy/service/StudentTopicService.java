package seig.wjy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import seig.wjy.domain.pojo.GraduationTopic;
import seig.wjy.domain.pojo.Information;
import seig.wjy.domain.pojo.User;
import seig.wjy.tools.ResponseResult;

public interface StudentTopicService extends IService<Information> {
    ResponseResult addStudentTopic(Information information);

    GraduationTopic getStudentTopic(Integer userId);
     ResponseResult deleteTopic(Integer userId) ;
}
