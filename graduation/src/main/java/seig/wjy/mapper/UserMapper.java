package seig.wjy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import seig.wjy.domain.pojo.User;

/**
 * @author wu
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    /**
     * 获取用户权限
     * @param username
     * @return
     */
    @Select("select role_name FROM sys_role WHERE id IN (\n" +
            "select is_belong FROM sys_user WHERE username=#{username})")
    String getAuth(String username);

    //获取教师名称
    @Select("select username from sys_user where id=#{userId}")
    String getTeacherName(int userId);
}
