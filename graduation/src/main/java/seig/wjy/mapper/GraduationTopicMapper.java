package seig.wjy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import seig.wjy.domain.pojo.GraduationTopic;
import seig.wjy.domain.pojo.Information;

import java.util.List;


/**
* @author wu
* @description 针对表【 graduation_topic】的数据库操作Mapper
* @createDate 2024-05-26 23:23:04
* @Entity wu.ju.yang.wujuyang.domain.pojo.GraduationTopic
*/

@Mapper
public interface GraduationTopicMapper extends BaseMapper<GraduationTopic> {
    /**
     * 自定义方法
     */
    @Select("select * from graduation_topic")
    public List<GraduationTopic> selectAll();
    @Select("select * from graduation_topic LIMIT #{pageSize} OFFSET #{pageNum}")
    public List<GraduationTopic> getTopics(int pageNum,int pageSize);

    @Update("update graduation_topic set status=0 where id=#{titleId}")
    public Integer updateStatus(Integer titleId);

    @Update("update graduation_topic set status=1 where id=#{titleId}")
    public Integer updateStatus1(Integer titleId);

    @Select("select * from graduation_topic where id=#{id}")
    public GraduationTopic selectOne(int id);

}




