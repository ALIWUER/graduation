package seig.wjy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import seig.wjy.domain.pojo.GraduationTopic;
import seig.wjy.domain.pojo.Information;


@Mapper
public interface StudentTopicMapper extends BaseMapper<Information> {
    @Select("select * from graduation_information where student_id=#{userId}")
    public Information selectOne(int userId);

    @Delete("delete from graduation_information where student_id=#{userId}")
    public int deleteByUserId(int userId);

}
