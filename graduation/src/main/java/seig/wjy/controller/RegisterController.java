package seig.wjy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import seig.wjy.domain.pojo.User;
import seig.wjy.service.LoginService;
import seig.wjy.service.RegisterService;
import seig.wjy.tools.AppHttpCodeEnum;
import seig.wjy.tools.ResponseResult;

import java.util.HashMap;
import java.util.Map;

@RestController
public class RegisterController {

    private RegisterService registerService;
    @Autowired
    public void setRegisterService(RegisterService registerService) {
        this.registerService = registerService;
    }

    @PostMapping("/register")
    public ResponseResult register(@RequestBody User user) {
        try {
            return registerService.register(user);
        }catch (Exception e){
            return ResponseResult.errorResult(AppHttpCodeEnum.SYSTEM_ERROR);
        }
    }

}
