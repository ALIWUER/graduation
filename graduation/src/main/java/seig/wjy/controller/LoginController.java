package seig.wjy.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import seig.wjy.domain.pojo.User;
import seig.wjy.domain.vo.LoginUser;
import seig.wjy.service.LoginService;
import seig.wjy.tools.AppHttpCodeEnum;
import seig.wjy.tools.ResponseResult;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;


    @PreAuthorize("hasAnyAuthority('ROLE_TEACHER')")
    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }
    @GetMapping("/niHao")
    public String niHao() {
        return "niHao";
    }



    @PostMapping("/login")
    public ResponseResult login(@RequestBody User user) {
        String jwt = loginService.login(user);
        if (StringUtils.hasLength(jwt)) {
            Map<String,String> result =new HashMap<>(1);
            User user1 = loginService.selectUser(user.getUsername());
            result.put("token",jwt);
            result.put("user", JSON.toJSONString(user1));
            return ResponseResult.okResult(result);
        }
        return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_ERROR);
    }

    @RequestMapping("/logout")
    public ResponseResult logout() {
        return ResponseResult.okResult(200, "logout");
    }
}
