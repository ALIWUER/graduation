package seig.wjy.controller;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import seig.wjy.domain.pojo.Information;
import seig.wjy.domain.pojo.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import seig.wjy.domain.pojo.GraduationTopic;
import seig.wjy.domain.qo.PageQO;
import seig.wjy.service.GraduationTopicService;
import seig.wjy.service.StudentTopicService;
import seig.wjy.tools.ResponseResult;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class StudentTopicController {
    @Resource
    StudentTopicService studentTopicService;
    //学生选题
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    @PostMapping("/graduation/addStudentTopic")
    public ResponseResult addStudentTopic(@RequestBody Information information) {
        return studentTopicService.addStudentTopic(information);
    }

    @PreAuthorize("hasRole('ROLE_STUDENT')")
    @GetMapping("/graduation/getStudentTopic")
    public ResponseResult getStudentTopic(Integer userId) {
        GraduationTopic graduationTopic=studentTopicService.getStudentTopic(userId);
        if(graduationTopic==null){
            return ResponseResult.errorResult(401,"您没有选题");
        }
        return  ResponseResult.okResult(graduationTopic);
    }
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    @GetMapping("/graduation/deleteTopic")
    public ResponseResult deleteTopic(Integer userId) {
        return studentTopicService.deleteTopic(userId);
    }

}
