package seig.wjy.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import seig.wjy.domain.pojo.GraduationTopic;
import seig.wjy.domain.qo.PageQO;
import seig.wjy.service.GraduationTopicService;
import seig.wjy.tools.ResponseResult;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class TopicController {

    @Resource
    GraduationTopicService graduationTopicService;


    @PreAuthorize("hasRole('ROLE_TEACHER')")
    @PostMapping("/graduation/topic")
    public ResponseResult addTopic(@RequestBody GraduationTopic graduationTopic) {
        return graduationTopicService.addTopic(graduationTopic);
    }

    //分页查询
    @PostMapping("/graduation/getTopic")
    public ResponseResult getTopic(@RequestBody PageQO qo) {
        IPage<GraduationTopic> graduationTopics = graduationTopicService.list(qo);
        return ResponseResult.okResult(graduationTopics);
    }

    //分页查询
    @CrossOrigin
    @PostMapping("/graduation/getList")
    public ResponseResult getList(@RequestBody PageQO qo) {
        IPage<GraduationTopic> graduationTopics = graduationTopicService.getList(qo);
        return ResponseResult.okResult(graduationTopics);
    }





}
