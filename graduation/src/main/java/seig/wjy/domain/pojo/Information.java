package seig.wjy.domain.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serial;
import java.io.Serializable;

@Data
@TableName("graduation_information")
public class Information  implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private int id;
    private int studentId;
    private int titleId;
    private int teacherId;
    private int status;
    private int selectNum;

    @TableField(exist = false)
    private int userId;

    @Serial
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
