package seig.wjy.domain.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 
 * @TableName  graduation_topic
 */
@TableName(value ="graduation_topic")
@Data
public class GraduationTopic implements Serializable {
    /**
     * 
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 题目名称
     */
    private String titleName;

    /**
     * 题目来源
     */
    private String titleSource;

    /**
     * 功能要求
     */
    private String titleRequirement;

    /**
     * 技术要求
     */
    private String technicalRequirement;

    /**
     * 指导老师 id
     */
    private Integer teacherId;

    /**
     * 面向专业 id
     */
    private Integer majorId;

    /**
     * 审批状态
     */
    private Integer status;

    @TableField(exist = false)
    private String teacherName;

    @Serial
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}