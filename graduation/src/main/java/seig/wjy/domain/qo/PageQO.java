package seig.wjy.domain.qo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageQO {
    private Integer pageNum;
    private Integer pageSize;
}
