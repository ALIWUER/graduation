package seig.wjy;

import io.jsonwebtoken.Claims;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import seig.wjy.mapper.UserMapper;
import seig.wjy.domain.pojo.User;
import seig.wjy.tools.JwtUtil;

import java.util.List;

@SpringBootTest
class GraduationApplicationTests {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void password() {
        String encode1 = passwordEncoder.encode("1234");
        String encode2 = passwordEncoder.encode("11");
        System.out.println(encode1);
        System.out.println(encode2);
        System.out.println(passwordEncoder.matches("1234", encode2));
    }

    @Test
    void contextLoads() {
        //生成jwt
        String jwt = JwtUtil.createJWT("2140739180","吴居洋", 60 * 1000L);
        System.out.println(jwt);
        System.out.println("------------");
        try {
            Claims claims = JwtUtil.parseJWT(jwt);
            String id = claims.getId();
            String subject = claims.getSubject();
            System.out.println(id+"=="+subject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    void userTest(){
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }

}
