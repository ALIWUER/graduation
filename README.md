# 毕业选题管理系统

## 使用技术
- [Vue](https://cn.vuejs.org/api/options-state.html)
- [element-plus](http://element-plus.org/zh-CN/component/overview.html)

post json

http://127.0.0.1:8888/graduation/addStudentTopic

{
"userId":2,
"titleId":1425585949,
"teacherId":"26",
"status":1
}

get

http://127.0.0.1:8888/graduation/getStudentTopic?userId=2